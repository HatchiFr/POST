#!/bin/bash

NGHOST="reader.com"
NGUSER="USERNAME"
NGPASS="PASSWORD"
NGGROUP="alt.binaries.******"
NGNBRCONNECT="10"
NGFROM="test@test.com" #laisser vide entrainera le random de celui-ci
NZBOUTPUT="/home/***/****" #Répertoire ou enregistrer le nzb
NZBTEMP="/nzbtemp/" 
PAR2BIN="parpar" #parpar ou bien par2
PAR2MEMORY="16000" #valuer en MB
PARPERCENT="10" #Ne pas mettre le %
MAXSEGNBR="8192" # Nombre de segments, maxi 32768
MAXRARNUMBER="99" # Nombre de rar maxi 10, 50, 99 selon les preferences
NYUUUSER="user"
LISTGROUP=("alt.binaries.gougouland" "alt.binaries.usenet2day" "alt.binaries.amazing" ) #en rajouter autant que vous voulez un groupe ou poster sera pioché au hasard dedans
#DATABASE
#si database = yes ne pas oublié de la set avec le fichier post.sql
DATABASE="YES" #YES or NO

U="sql user"
P="sql pass"
D="sql database"
H="sql host"

